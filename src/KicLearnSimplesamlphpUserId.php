<?php

namespace Drupal\kic_learn;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\UserInterface;
use SAML2\Constants;
use SimpleSAML\Configuration;
use SimpleSAML\Metadata\MetaDataStorageHandler;
use SimpleSAML\Module\drupalauth\DrupalHelper;
use SimpleSAML\Module\saml\Auth\Process\PersistentNameID;

/**
 * Interface for user ID providers.
 */
class KicLearnSimplesamlphpUserId implements KicLearnUserIdProviderInterface {

  /**
   * The module's configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory) {

    $this->config = $config_factory->get('kic_learn.settings');
    $this->logger = $logger_factory->get('kic_learn');
  }

  /**
   * Get the ID of the user used by the learning backend.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user for whom to return the learning backend ID.
   *
   * @return string
   *   The ID of the user that is used by the learning backend API or an empty
   *   string, if the ID failed to load.
   */
  public function getUserId(UserInterface $user) {
    $idp_id = $this->config->get('saml20_idp');
    $sp_id = $this->config->get('saml20_sp');
    if (empty($idp_id) || empty($sp_id)) {
      $this->logger->error('Missing SAML IdP or SP. Unable to retrieve SAML user ID.');
      return '';
    }

    $config = [];
    $metadata_handler = MetaDataStorageHandler::getMetadataHandler();
    $metadata = $metadata_handler->getMetaData($idp_id, 'saml20-idp-hosted');
    if (isset($metadata['authproc'])) {
      foreach ($metadata['authproc'] as $authproc) {
        if (is_array($authproc) && isset($authproc['class'])
          && $authproc['class'] === 'saml:PersistentNameID') {

          if (substr(Configuration::VERSION, 0, 2) === '1.') {
            $config['attribute'] = $authproc['attribute'] ?? [];
          }
          elseif (substr(Configuration::VERSION, 0, 2) === '2.') {
            $config['identifyingAttribute'] = $authproc['identifyingAttribute'] ?? [];
          }
        }
      }
    }
    if (empty($config['attribute']) && empty($config['identifyingAttribute'])) {
      $this->logger->error('Empty attribute for persistent name ID. Unable to retrieve SAML user ID.');
      return '';
    }

    $state = [
      'Source' => ['entityid' => $idp_id],
      'Destination' => ['entityid' => $sp_id],
      'Attributes' => [],
    ];
    $ssp_config = $metadata_handler->getMetaDataConfig($idp_id, 'saml20-idp-hosted');
    $auth = $ssp_config->getString('auth');
    if ($auth === 'drupal-userpass') {
      $ssp_authsources = Configuration::getConfig('authsources.php');
      $helper = new DrupalHelper();
      $requested_attr = $ssp_authsources->getValue($auth)['attributes'];
      $state['Attributes'] = $helper->getAttributes($user, $requested_attr);
    }
    if (empty($state['Attributes'])) {
      $this->logger->error('Empty user attributes. Unable to retrieve SAML user ID.');
      return '';
    }

    $persistent_name_id = new PersistentNameID($config, NULL);
    $persistent_name_id->process($state);
    $name_id = $state['saml:NameID'][Constants::NAMEID_PERSISTENT] ?? NULL;

    if ($name_id !== NULL) {
      return $name_id->getValue();
    }
    return '';
  }

}
