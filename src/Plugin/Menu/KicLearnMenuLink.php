<?php

namespace Drupal\kic_learn\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Url;

/**
 * Provides a default implementation for menu link plugins.
 */
class KicLearnMenuLink extends MenuLinkDefault {

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {
    $options = $this->getOptions();
    $description = $this->getDescription();
    if ($title_attribute && $description) {
      $options['attributes']['title'] = $description;
    }
    $target = $this->pluginDefinition['url'];
    if ($target === 'dashboard') {
      return kic_learn_dashboard_url($options);
    }
    elseif ($target === 'moodle') {
      return kic_learn_moodle_url($options);
    }
    elseif ($target === 'certificates') {
      return kic_learn_certificates_url($options);
    }
    elseif ($target === 'settings') {
      return kic_learn_settings_url($options);
    }
    else {
      return Url::fromUri($this->pluginDefinition['url'], $options);
    }
  }

}
