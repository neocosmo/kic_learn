<?php

namespace Drupal\kic_learn\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\kic_learn\KicLearnUserIdProviderFactory;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;

/**
 * API client for the KI-Campus learning backend.
 */
class KicLearnConnector {

  /**
   * The module's configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * User ID provider.
   *
   * @var \Drupal\kic_learn\KicLearnUserIdProviderInterface
   */
  protected $userIdProvider;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructor.
   *
   * @param \Drupal\kic_learn\KicLearnUserIdProviderFactory $user_id_factory
   *   A user ID provider factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    KicLearnUserIdProviderFactory $user_id_factory,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    Connection $database
  ) {
    $this->userIdProvider = $user_id_factory->get();
    $this->config = $config_factory->get('kic_learn.settings');
    $this->logger = $logger_factory->get('kic_learn');
    $this->database = $database;
  }

  /**
   * Get the user data for the learning backend.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to get the data from.
   *
   * @return array
   *   The data of the user for sending to the learning backend.
   */
  public function getUserData(UserInterface $user) {
    $data = [];
    $email = $user->getEmail();
    if (!empty($email)) {
      $data['email'] = $email;
    }
    $langcode = $user->getPreferredLangcode();
    if (!empty($langcode)) {
      $data['language'] = $langcode;
    }
    if (!empty($user->get('user_picture')->first())) {
      $picture = $user->get('user_picture')->first();
      $main_prop = $picture->mainPropertyName();
      $file = File::load($picture->getValue()[$main_prop]);
      $data['avatar'] = $file->createFileUrl(FALSE);
    }
    else {
      $data['avatar'] = NULL;
    }
    if (!empty($user->get('field_birthday')->first())) {
      $birthday = $user->get('field_birthday')->first();
      $main_prop = $birthday->mainPropertyName();
      $data['born_at'] = $birthday->getValue()[$main_prop];
    }
    if (!empty($user->get('field_firstname')->first())
      && !empty($user->get('field_lastname')->first())) {

      $name = $user->get('field_firstname')->first();
      $main_prop = $name->mainPropertyName();
      $data['full_name'] = $name->getValue()[$main_prop];

      $data['full_name'] .= ' ';

      $name = $user->get('field_lastname')->first();
      $main_prop = $name->mainPropertyName();
      $data['full_name'] .= $name->getValue()[$main_prop];
    }
    return $data;
  }

  /**
   * Update the user data at the learning backend.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to update.
   * @param array $data
   *   (Optional) The data of the user, that should be updated. If an empty
   *   array is provided, the data is retrieved from $user.
   */
  public function userUpdate(UserInterface $user, array $data = []) {
    $user_id = $this->userIdProvider->getUserId($user);
    if (empty($user_id)) {
      $this->logger->error('Missing learning backend user ID. Unable to update user data.');
      return;
    }

    $headers = $this->getHeaders();
    $headers['Accept'] = 'application/vnd.openhpi.user+json;v=1.0';
    $headers['Content-Type'] = 'application/json';

    if (empty($data)) {
      $data = $this->getUserData($user);
    }
    if (empty($data)) {
      return;
    }

    $data_stream = Utils::streamFor(json_encode($data));
    $request = new Request('PATCH', 'users/' . $user_id, $headers, $data_stream);
    $client = $this->getClient();
    if (!$client) {
      $this->logger->error('Failed to get client object. Unable to update user data.');
      return;
    }
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending PATCH request to update user data of user @u at the learning backend threw an exception: @e', [
        '@u' => $user->id(),
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $this->logger->info('User data update of user @u responded with status code @s.', [
        '@u' => $user->id(),
        '@s' => $response->getStatusCode(),
      ]);
    }
  }

  /**
   * Retrieve the information of a single specific course.
   *
   * @param string $kic_uuid
   *   The UUID of the course at the learning backend.
   *
   * @return object
   *   The course information.
   */
  public function getCourse(string $kic_uuid) {
    if (empty($kic_uuid)) {
      $this->logger->error('Got an empty UUID. Failed to retrieve course information.');
      return NULL;
    }
    $headers = $this->getHeaders();
    $headers['Accept'] = 'application/vnd.openhpi.course+json;v=1.1';
    $request = new Request('GET', 'courses/' . $kic_uuid, $headers);
    $client = $this->getClient();
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending GET request to retrieve course data of course @c at the learning backend threw an exception: @e', [
        '@c' => $kic_uuid,
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $status = $response->getStatusCode();
      if ($status == 200) {
        return json_decode($response->getBody());
      }
      else {
        $this->logger->error('kic_learn: Failed to retrieve course data. Status @s; Body: @b',
          ['@s' => $status, '@b' => $response->getBody()]);
      }
    }
    return NULL;
  }

  /**
   * Get a list of all courses' UUIDs.
   *
   * @return array
   *   A list of course KIC UUIDs.
   */
  public function getCoursesUuids() {
    $headers = $this->getHeaders();
    $headers['Accept'] = 'application/vnd.openhpi.list+json;v=1.1';
    $request = new Request('GET', 'courses', $headers);
    $client = $this->getClient();
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending GET request to retrieve all courses threw an exception: @e', [
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $status = $response->getStatusCode();
      if ($status == 200) {
        $data = json_decode($response->getBody());
        $list = array_map(function ($i) {
          return substr($i->url, -36);
        }, $data->items);
        return $list;
      }
      else {
        $this->logger->error('kic_learn: Failed to retrieve course data. Status @s; Body: @b',
          ['@s' => $status, '@b' => $response->getBody()]);
      }
    }
    return [];
  }

  /**
   * Get a client object for connecting to the learning backend.
   *
   * @return \GuzzleHttp\Client
   *   The client object or NULL, if creation of the object failed.
   */
  protected function getClient() {
    $base_url = kic_learn_base_url();
    if ($base_url === NULL) {
      $this->logger->error('Missing base URL of learning backend.');
      return NULL;
    }
    $base_path = $this->config->get('api_base_path');
    $api_base = $base_url->toString() . $base_path;
    if (substr($api_base, -1) !== '/') {
      $api_base .= '/';
    }
    return new Client(['base_uri' => $api_base]);
  }

  /**
   * Get the basic HTTP headers for requests to the learning backend.
   *
   * @return array
   *   An array of headers, that can be passed to the request object. Any
   *   headers used for authentication are present, according the the module's
   *   configuration.
   */
  protected function getHeaders() {
    $headers = [];
    $bearer_token = $this->config->get('api_auth_bearer_token');
    if (!empty($bearer_token)) {
      $headers['Authorization'] = 'Bearer ' . $bearer_token;
    }
    return $headers;
  }

  /**
   * Get the enrolled courses achievements for a user.
   *
   * @param string $user_id
   *   The user ID.
   *
   * @return array
   *   The enrolled courses achievements.
   */
  public function getEnrolledCoursesAchv(string $user_id) {
    if (empty($user_id)) {
      $this->logger->error('Got an empty user ID. Failed to retrieve course achievements.');
      return NULL;
    }
    $user_token = $this->getUserAuthToken($user_id);
    if (empty($user_token)) {
      $this->logger->error('An error occurred while retrieving user authentication token. Failed to retrieve course achievements.');
      return NULL;
    }

    $headers['Authorization'] = $this->getUserAuthToken($user_id);
    $headers['Accept'] = 'application/json';
    $request = new Request('GET', 'my_courses', $headers);
    $client = $this->getTempClient();
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending GET request to retrieve all enrolled courses achievements threw an exception: @e', [
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $status = $response->getStatusCode();
      if ($status == 200) {
        $data = json_decode($response->getBody());
        return $data;
      }
      else {
        $this->logger->error('kic_learn: Failed to retrieve enrolled courses achievements. Status @s; Body: @b',
          ['@s' => $status, '@b' => $response->getBody()]);
      }
    }
    return [];
  }

  /**
   * Get the course achievements for a user.
   *
   * @param string $user_id
   *   The user ID.
   * @param string $kic_uuid
   *   The UUID of the course at the learning backend.
   *
   * @return object
   *   The course achievements.
   */
  public function getEnrolledCourseAchv(string $user_id, string $kic_uuid) {
    if (empty($kic_uuid)) {
      $this->logger->error('Got an empty UUID. Failed to retrieve course achievements.');
      return NULL;
    }
    if (empty($user_id)) {
      $this->logger->error('Got an empty user ID. Failed to retrieve course achievements.');
      return NULL;
    }
    $user_token = $this->getUserAuthToken($user_id);
    if (empty($user_token)) {
      $this->logger->error('An error occurred while retrieving user authentication token. Failed to retrieve course achievements.');
      return NULL;
    }

    $headers['Authorization'] = $this->getUserAuthToken($user_id);
    $headers['Accept'] = 'application/json';
    $request = new Request('GET', 'my_courses/' . $kic_uuid . '/achievements', $headers);
    $client = $this->getTempClient();
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending GET request to retrieve course achievements threw an exception: @e', [
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $status = $response->getStatusCode();
      if ($status == 200) {
        $data = json_decode($response->getBody());
        return $data;
      }
      else {
        $this->logger->error('kic_learn: Failed to retrieve course achievements. Status @s; Body: @b',
          ['@s' => $status, '@b' => $response->getBody()]);
      }
    }
    return NULL;
  }

  /**
   * Get the user authentication token.
   *
   * @param string $user_id
   *   The user ID.
   *
   * @return string
   *   The user authentication token.
   */
  public function getUserAuthToken(string $user_id) {
    if (empty($kic_uuid)) {
      $this->logger->error('Got an empty user ID. Failed to retrieve user authentication token.');
      return NULL;
    }
    $headers = $this->getTempHeaders();
    $headers['Accept'] = 'application/json';
    $headers['Content-Type'] = 'multipart/form-data; boundary=---011000010111000001101001';
    $headers['form_params'] = [
      'uid' => $user_id,
    ];
    $request = new Request('POST', 'authenticate', $headers);
    $client = $this->getTempClient();
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending POST request to retrieve user authentication token threw an exception: @e', [
        '@e' => $e->getMessage(),
      ]);
    }

    if ($response !== NULL) {
      $status = $response->getStatusCode();
      if ($status == 200) {
        $data = json_decode($response->getBody());
        return $data;
      }
      else {
        $this->logger->error('kic_learn: Failed to retrieve user authentication token. Status @s; Body: @b',
          ['@s' => $status, '@b' => $response->getBody()]);
      }
    }
    return NULL;
  }

  /**
   * Get a temporary client object(/bridges/chatbot).
   *
   * @return \GuzzleHttp\Client
   *   The client object or NULL, if creation of the object failed.
   */
  protected function getTempClient() {
    $api_base = 'https://learn-staging.ki-campus.org/bridges/chatbot/';
    return new Client(['base_uri' => $api_base]);
  }

  /**
   * Test the API by making a GET request to a specified endpoint.
   *
   * @return mixed
   *   The response from the API, decoded from JSON into a PHP variable.
   */
  public function testApi() {
    $headers = $this->getHeaders();
    $headers['Accept'] = 'application/vnd.openhpi.user+json;v=1.0';
    $headers['Content-Type'] = 'application/json';

    $request = new Request('GET', 'your_endpoint_here', $headers);
    $client = $this->getClient();
    if (!$client) {
      $this->logger->error('Failed to get client object. Unable to test API.');
      return;
    }
    $response = NULL;
    try {
      $response = $client->send($request);
    }
    catch (\Exception $e) {
      $this->logger->error('Sending GET request to test API threw an exception: @e', [
        '@e' => $e->getMessage(),
      ]);
    }
    if ($response !== NULL) {
      $this->logger->info('API test responded with status code @s.', [
        '@s' => $response->getStatusCode(),
      ]);
      return json_decode($response->getBody());
    }
  }

  /**
   * Get the basic HTTP headers for requests to a custom API (/bridges/chatbot).
   *
   * @return array
   *   An array of headers, that can be passed to the request object. Any
   *   headers used for authentication are present, according the module's
   *   configuration.
   */
  protected function getTempHeaders() {
    $headers = [];
    $bearer_token_staging = '';
    $bearer_token_production = '';
    $bearer_token = $bearer_token_staging || $bearer_token_production;
    $headers['Authorization'] = 'Bearer ' . $bearer_token;
    return $headers;
  }

  /**
   * Get the enrolled courses and progress for a user from the Moodle API.
   *
   * @param string $user_id
   *   The user ID.
   *
   * @return array
   *   The enrolled courses and progress.
   */
  public function getMoodleCourses(string $user_id) {
    if (empty($user_id)) {
      $this->logger->error('Got an empty user ID. Failed to retrieve course.');
      return NULL;
    }
    $user_name = ($user_id == '1') ? 'superadmin' : $user_id;

    $headers = [
      'Authorization' => 'Bearer ' . 'T6hzpTJdFooIKh42kFgyPi0FM69lZ2CH',
      'Accept' => 'application/json',
    ];

    $params = [
      'user_name' => $user_name,
    ];

    $client = new Client();
    $response = $client->request('GET', 'https://ki-campus.moodle.staging.fernuni-hagen.de/local/ki_enrollment/enrollment.php', [
      'headers' => $headers,
      'query' => $params,
    ]);

    if ($response->getStatusCode() == 200) {
      return json_decode($response->getBody(), TRUE);
    }
    else {
      $this->logger->error('Failed to retrieve Moodle courses. Status: @status; Body: @body', [
        '@status' => $response->getStatusCode(),
        '@body' => $response->getBody(),
      ]);
      return NULL;
    }
  }

  /**
   * Retrieves completed courses for the current user.
   *
   * @param int $uid
   *   The UID of the currently logged-in user.
   *
   * @return array
   *   An array of completed courses with titles and verification links.
   */
  public function getUserCompletedCourses(int $uid): array {

    $completed_courses = [];

    try {
      $schema = $this->database->schema();
      if (!$schema->tableExists('dump_course_details')) {
        $this->logger->error('The table "dump_course_details" does not exist in the database.');
        return $completed_courses;
      }

      $query = $this->database->select('dump_course_details', 'dcd')
        ->fields('dcd', [
          'course_id',
          'title',
          'user_id',
          'kic_user_id',
          'verification',
        ])
        ->condition('kic_user_id', $uid, '=');

      $results = $query->execute()->fetchAll();

      foreach ($results as $row) {
        $completed_courses[] = [
          'course_id' => $row->course_id,
          'title' => $row->title,
          'verification_link' => 'https://learn.ki-campus.org/verify/' . $row->verification,
        ];
      }
    }
    catch (\Exception $e) {
      $this->logger->error('An error occurred while retrieving user completed courses: @message', [
        '@message' => $e->getMessage(),
      ]);
    }

    return $completed_courses;
  }

}
