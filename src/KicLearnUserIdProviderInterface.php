<?php

namespace Drupal\kic_learn;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\UserInterface;

/**
 * Interface for user ID providers.
 */
interface KicLearnUserIdProviderInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory);

  /**
   * Get the ID of the user used by the learning backend.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user for whom to return the learning backend ID.
   *
   * @return string
   *   The ID of the user that is used by the learning backend API.
   */
  public function getUserId(UserInterface $user);

}
