<?php

namespace Drupal\kic_learn\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use SimpleSAML\Metadata\MetaDataStorageHandler;

/**
 * Module configuration form.
 */
class KicLearnForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'kic_learn.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kic_learn_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config(static::SETTINGS);

    /*
     * Do not use the object's settings here, because these only include the
     * settings submitted via this form, but not those settings set via the
     * settings.php-file.
     */
    $api_auth_bearer_token = \Drupal::config(static::SETTINGS)->get('api_auth_bearer_token');
    if (empty($api_auth_bearer_token)) {
      $form['api_auth_bearer_token'] = [
        '#type' => 'container',
        'label' => [
          '#type' => 'html_tag',
          '#tag' => 'label',
          '#value' => $this->t('API Auth Bearer Token missing!'),
        ],
        'text' => [
          '#type' => 'html_tag',
          '#tag' => 'description',
          '#value' => $this->t("Provide a Bearer Token for API authentication via the <code>settings.php</code> via the following code:<br><code>\$config['@setting']['api_auth_bearer_token'] = '...';</code>", [
            '@setting' => static::SETTINGS,
          ]),
        ],
      ];
    }
    else {
      $form['api_auth_bearer_token'] = [
        '#type' => 'container',
        'label' => [
          '#type' => 'html_tag',
          '#tag' => 'label',
          '#value' => $this->t('API Auth Bearer Token'),
        ],
        'text' => [
          '#type' => 'html_tag',
          '#tag' => 'description',
          '#value' => $this->t('The Bearer Token is set via the <code>settings.php</code>.'),
        ],
      ];
    }

    $form['backend_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Learning Backend URL'),
      '#default_value' => $config->get('backend_url'),
      '#description' => $this->t('URL of the learning backend.'),
      '#required' => TRUE,
    ];
    $form['dashboard_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Dashboard Path'),
      '#default_value' => $config->get('dashboard_path'),
      '#description' => $this->t('Path of the user dashboard. This is appended to the <em>Learning Backend URL</em>.'),
    ];
    $form['certificates_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Certificates Path'),
      '#default_value' => $config->get('certificates_path'),
      '#description' => $this->t('Path of the user certificates. This is appended to the <em>Learning Backend URL</em>.'),
    ];
    $form['settings_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Settings Path'),
      '#default_value' => $config->get('settings_path'),
      '#description' => $this->t('Path of the user settings page. This is appended to the <em>Learning Backend URL</em>.'),
    ];
    $form['course_enroll_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Course Enroll Path'),
      '#default_value' => $config->get('course_enroll_path'),
      '#description' => $this->t('Path for course enrollment. You may use <em>[course]</em> as wildcard for a course identifier. This is appended to the <em>Learning Backend URL</em>.'),
    ];
    $form['api_base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base Path'),
      '#default_value' => $config->get('api_base_path'),
      '#description' => $this->t("Base path of learning backend's API. This is appended to the the <em>Learning Backend URL</em>."),
    ];
    $form['language_parameter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language parameter name'),
      '#default_value' => $config->get('language_parameter'),
      '#description' => $this->t('URL query parameter for determining the language of the Learning Backend. This is appended to the <em>Learning Backend URL</em>.'),
    ];
    $form['moodle_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moodle URL'),
      '#default_value' => $config->get('moodle_url'),
      '#description' => $this->t('URL of Moodle.'),
      '#required' => TRUE,
    ];
    $form['moodle_enroll_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moodle Enroll Path'),
      '#default_value' => $config->get('moodle_enroll_path'),
      '#description' => $this->t('Path for Moodle course enrollment. You may use <em>[course]</em> as wildcard for a course identifier. This is appended to the <em>Moodle URL</em>.'),
    ];

    $idp_options = ['none' => $this->t('None')] + $this->getSamlIdps();
    $form['saml20_idp'] = [
      '#type' => 'select',
      '#title' => $this->t('SAML Identity Provider'),
      '#description' => $this->t('Select the identity provider which is used for the learning backend SSO.'),
      '#options' => $idp_options,
      '#default_value' => $config->get('saml20_idp', 'none'),
    ];
    $sp_options = ['none' => $this->t('None')] + $this->getSamlSps();
    $form['saml20_sp'] = [
      '#type' => 'select',
      '#title' => $this->t('SAML Service Provider'),
      '#description' => $this->t('Select the learning backend service provider.'),
      '#options' => $sp_options,
      '#default_value' => $config->get('saml20_sp', 'none'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url = $form_state->getValue('backend_url');
    try {
      // Try to create a URL object from the string. If it succeeds, we are
      // good.
      Url::fromUri($url);
    }
    catch (InvalidArgumentException $e) {
      $form_state->setErrorByName('backend_url', $this->t('Malformed URL.'));
    }
    $idp = $form_state->getValue('saml20_idp');
    if ($idp === 'none') {
      $form_state->setValue('saml20_idp', '');
    }
    elseif (!isset($this->getSamlIdps()[$idp])) {
      $form_state->setErrorByName('saml20_idp', $this->t('Invalid identity provider.'));
    }
    $sp = $form_state->getValue('saml20_sp');
    if ($sp === 'none') {
      $form_state->setValue('saml20_sp', '');
    }
    elseif (!isset($this->getSamlSps()[$sp])) {
      $form_state->setErrorByName('saml20_sp', $this->t('Invalid service provider.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $config->set('backend_url', $form_state->getValue('backend_url'));
    $config->set('dashboard_path', $form_state->getValue('dashboard_path'));
    $config->set('certificates_path', $form_state->getValue('certificates_path'));
    $config->set('settings_path', $form_state->getValue('settings_path'));
    $config->set('course_enroll_path', $form_state->getValue('course_enroll_path'));
    $config->set('api_base_path', $form_state->getValue('api_base_path'));
    $config->set('language_parameter', $form_state->getValue('language_parameter'));
    $config->set('moodle_url', $form_state->getValue('moodle_url'));
    $config->set('moodle_enroll_path', $form_state->getValue('moodle_enroll_path'));
    $config->set('saml20_idp', $form_state->getValue('saml20_idp'));
    $config->set('saml20_sp', $form_state->getValue('saml20_sp'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Get a list of SAML identity providers.
   *
   * @return array
   *   An array of identity provider IDs keyed by themselves.
   */
  protected function getSamlIdps() {
    $metadata_handler = MetaDataStorageHandler::getMetadataHandler();
    $idps = $metadata_handler->getList('saml20-idp-hosted');
    $idp_ids = array_map(function ($idp) {
      return $idp['entityid'];
    }, $idps);
    return array_combine($idp_ids, $idp_ids);
  }

  /**
   * Get a list of SAML service providers.
   *
   * @return array
   *   An array of service provider IDs keyed by themselves.
   */
  protected function getSamlSps() {
    $metadata_handler = MetaDataStorageHandler::getMetadataHandler();
    $sps = $metadata_handler->getList('saml20-sp-remote');
    $sp_ids = array_map(function ($sp) {
      return $sp['entityid'];
    }, $sps);
    return array_combine($sp_ids, $sp_ids);
  }

}
