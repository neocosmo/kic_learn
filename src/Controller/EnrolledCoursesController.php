<?php

namespace Drupal\kic_learn\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\kic_learn\Service\KicLearnConnector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Enrolled Courses routes.
 */
class EnrolledCoursesController extends ControllerBase {

  /**
   * The KicLearnConnector service.
   *
   * @var \Drupal\kic_learn\Service\KicLearnConnector
   */
  protected $kicLearnConnector;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(KicLearnConnector $kic_learn_connector, EntityTypeManagerInterface $entity_type_manager) {
    $this->kicLearnConnector = $kic_learn_connector;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('kic_learn.api_connector'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Builds the response.
   */
  public function content() {
    $user = $this->currentUser();
    $courses = $this->kicLearnConnector->getEnrolledCoursesAchv($user->id());

    // Render the courses as a list.
    $items = [];
    foreach ($courses as $course) {
      $items[] = $this->t('Course ID: @id, Achievements: @achievements, Enrollment Date: @date, Unenrolled: @unenrolled', [
        '@id' => $course->course_id,
        '@achievements' => implode(', ', $course->achievements),
        '@date' => $course->enrollment_date,
        '@unenrolled' => $course->unenrolled ? $this->t('Yes') : $this->t('No'),
      ]);
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => $this->t('Enrolled Courses'),
    ];
  }

  /**
   * Builds the response for Moodle courses.
   */
  public function moodleCourses() {
    // Get the current user ID.
    $user_id = $this->currentUser()->id();

    // Get the Moodle courses for this user.
    $courses = $this->kicLearnConnector->getMoodleCourses($user_id);

    // Check if the courses were retrieved successfully.
    if ($courses === NULL || empty($courses['data'])) {
      return [
        '#markup' => $this->t('You are not enrolled in any courses at this time.'),
      ];
    }

    // Create a render array to display the courses.
    $render_array = [
      '#theme' => 'item_list',
      '#items' => [],
      '#title' => $this->t('Moodle Courses'),
    ];

    // Add each course to the render array.
    foreach ($courses['data'] as $course) {
      // Check if course_id is valid.
      if (empty($course['course_id'])) {
        continue;
      }

      // Load nodes based on the Moodle course ID.
      $nodes = $this->entityTypeManager
        ->getStorage('node')
        ->loadByProperties(['field_moodle_course_id' => $course['course_id']]);

      // If a course content was found, get the title from it.
      $title = !empty($nodes) ? reset($nodes)->getTitle() : $this->t('Unknown Course');

      // Add course information to the render array.
      $render_array['#items'][] = $this->t('Course ID: @id, Title: @title, Completed: @completed, Progress: @progress, Grade: @grade', [
        '@id' => $course['course_id'],
        '@title' => $title,
        '@completed' => $course['completed'] ? $this->t('Yes') : $this->t('No'),
        '@progress' => $course['progress'],
        '@grade' => $course['grade'],
      ]);
    }

    return $render_array;
  }

  /**
   * Builds the response for TITLE of courses.
   */
  public function getNodeIdFromTitle($course_title) {
    $query = Database::getConnection()->select('node_field_data', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.title', $course_title, '=');
    $query->condition('n.type', 'course', '=');
    $query->condition('n.status', 1, '=');
    $result = $query->execute()->fetchField();

    return $result ? (int) $result : NULL;
  }

  /**
   * Builds the response for list of courses.
   */
  public function userCompletedCourses() {
    $user = $this->currentUser();

    // Check if the user is logged in.
    if ($user->isAnonymous()) {
      return [
        '#markup' => $this->t('Access denied. You must be logged in to view completed courses.'),
      ];
    }

    $uid = (int) $user->id();

    // Call the service method to fetch completed courses.
    $completed_courses = $this->kicLearnConnector->getUserCompletedCourses($uid);

    // Keep track of unique verification links.
    $unique_links = [];
    $items = [];
    $courses = [];

    foreach ($completed_courses as $course) {
      if (in_array($course['verification_link'], $unique_links)) {
        continue;
      }
      $unique_links[] = $course['verification_link'];

      $nid = $this->getNodeIdFromTitle($course['title']);

      $course_url = $nid ? "/node/{$nid}" : NULL;

      $courses[] = [
        'title' => $course['title'],
        'url' => $course_url,
        'verification_url' => $course['verification_link'],
      ];

      if ($course_url !== NULL) {
        $items[] = $this->t('<a href="@course_url">@title</a> - Verification: <a href="@link">@link</a>', [
          '@course_url' => $course_url,
          '@title' => $course['title'],
          '@link' => $course['verification_link'],
        ]);
      }
      else {
        $items[] = $this->t('@title - Verification: <a href="@link">@link</a>', [
          '@title' => $course['title'],
          '@link' => $course['verification_link'],
        ]);
      }
    }

    // Return the rendered list or a message if there are no courses.
    if (empty($items)) {
      return [
        '#markup' => $this->t('No certificates found.'),
      ];
    }

    return [
      '#theme' => 'course_completion_list',
      '#items' => $items,
      '#title' => $this->t('My certificates'),
      '#description' => $this->t('Below is a list of all the courses you have completed. Each course has a verification link to confirm your achievement.'),
      '#courses' => $courses,
      '#verification_label' => $this->t('Proof of achievement'),
    ];

  }

  /**
   * Builds the response for filtered Moodle courses using the teaser view mode.
   */
  public function myPageCourses() {
    $user = $this->currentUser();
    $user_id = $user->id();

    if (empty($user_id)) {
      return [
        '#markup' => $this->t('An error occurred. Please try again later.'),
      ];
    }

    $courses = $this->kicLearnConnector->getMoodleCourses($user_id);

    if ($courses === NULL || empty($courses['data'])) {
      return [
        '#markup' => $this->t('You are not enrolled in any courses at this time.'),
      ];
    }

    $in_progress_courses = [];
    $completed_courses = [];

    foreach ($courses['data'] as $course) {
      if (empty($course['course_id'])) {
        continue;
      }

      $nodes = $this->entityTypeManager
        ->getStorage('node')
        ->loadByProperties(['field_moodle_course_id' => $course['course_id']]);

      if (!empty($nodes)) {
        $node = reset($nodes);
        $title = $node->getTitle();
        $nid = $node->id();
        $node_url = $nid
        ? $this->t(
            '<a href=":url">:title</a>',
            [
              ':url' => "/node/$nid",
              ':title' => $title,
            ]
          )
        : $title;
        if ($course['grade'] < 0.6) {
          $in_progress_courses[] = $this->entityTypeManager->getViewBuilder('node')->view($node, 'teaser');
        }
        if ($course['grade'] >= 0.6 && $course['progress'] < 90) {
          $in_progress_courses[] = $this->entityTypeManager->getViewBuilder('node')->view($node, 'teaser');
          $completed_courses[] = $this->t('@node_title', [
            '@node_title' => $node_url,
          ]);
        }
        if ($course['progress'] >= 90) {
          $completed_courses[] = $this->t('@node_title', [
            '@node_title' => $node_url,
          ]);
        }
      }
    }

    if (empty($in_progress_courses)) {
      $in_progress_courses[] = [
        '#markup' => $this->t('No ongoing courses at the moment.'),
      ];
    }

    if (empty($completed_courses)) {
      $completed_courses[] = [
        '#markup' => $this->t('No certificates available at this time.'),
      ];
    }

    return [
      '#theme' => 'mypage_courses',
      '#in_progress_courses' => $in_progress_courses,
      '#completed_courses' => $completed_courses,
    ];
  }

}
