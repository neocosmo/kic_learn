<?php

namespace Drupal\kic_learn;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Factory class for user ID provider.
 */
class KicLearnUserIdProviderFactory {

  /**
   * A configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory to retrieve the module's configuration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory) {

    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Get a user ID provider.
   *
   * @return \Drupal\kic_learn\KicLearnUserIdProviderInterface
   *   A user ID provider.
   *
   * @todo Should the ID that is used by the learning backend change in the
   * future, we need to implement different implementations of the ID provider
   * and make their selection configurable.
   */
  public function get() {
    return new KicLearnSimplesamlphpUserId($this->configFactory,
      $this->loggerFactory);
  }

}
